package main

type Customer struct {
	Name, Address string
	Age int
}

func (customer Customer) sayHi(name string) {
	println("Hello", name, "My name is", customer.Name)
}

func main() {
	var ekky Customer
	ekky.Name = "Ekky Kharismadhany"
	ekky.Address = "Bojonegoro"
	ekky.Age = 21
	ekky.sayHi("Ekky")

}
