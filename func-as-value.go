package main

func getGoodBye(name string) string {
	return "Good bye " + name
}

func main() {
	words := getGoodBye("Ekky Kharismadhany")
	println(words)
}
