package main

type Filter func(string) string

func sayUserName(name string, filter Filter) string {
	return "Hello " + filter(name)
}

func filterName(name string) string {
	if name == "Anjing" {
		return "..."
	} else {
		return name
	}
}

func main() {
	result := sayUserName("Dogi", filterName)
	println(result)
}
