package main

import "fmt"

func main() {
	const (
		firstName = "Ekky"
		lastName = "Kharismadhany"
	)

	fmt.Println(firstName + " " + lastName)
}
